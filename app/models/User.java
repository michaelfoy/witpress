package models;

import play.db.jpa.Model;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.OneToMany;

/**
 * @file User.java
 * @brief Class which describes a user
 * @author michaelfoy
 * @version 2016-06-29
 */
@Entity
public class User extends Model {
  public String email;
  public String firstName;
  public String lastName;
  public String password;
  
  //Maps user to it's relevant posts, when user deleted, so too are it's posts
  @OneToMany (mappedBy = "blogger", cascade=CascadeType.REMOVE)
  public List<Post> posts;

  /**
   * Constructs a User object
   * 
   * @param firstName User's first name
   * @param lastName User's last name
   * @param email User's email
   * @param password User's password
   */
  public User(String firstName, String lastName, String email, String password) {
    this.firstName = firstName;
    this.lastName = lastName;
    this.email = email;
    this.password = password;
    this.posts = new ArrayList<Post>();
  }

  /**
   * Finds a user using their email address
   * 
   * @param email The email address of the user being searched
   * @return The relevant user for the email address
   */
  public static User findByEmail(String email) {
    return find("email", email).first();
  }

  /**
   * Checks if a given password matches a User's password
   * 
   * @param password The password to be checked
   * @return True if the password is correct for this user
   */
  public boolean checkPassword(String password) {
    return this.password.equals(password);
  }
}
