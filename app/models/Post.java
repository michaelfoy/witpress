package models;

import play.db.jpa.Model;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.CascadeType;
import controllers.Accounts;

/**
 * @file Post.java
 * @brief Class which describes a post
 * @author michaelfoy
 * @version 2016-06-29
 */

@Entity
public class Post extends Model {
  
  public String title;
  public String content;
  public String date;
  
  @ManyToOne
  public User blogger;
  
  // Maps post to it's relevant comments, when post deleted, so too are it's comments
  @OneToMany (mappedBy = "commentedPost", cascade=CascadeType.REMOVE)
  List<Comment> comments = new ArrayList<Comment>();
  
  /**
   * Default Post constructor including user information and welcome message
   */
  public Post() {
    this.title = "Welcome to WITPress";
    this.content = "To start publishing your WITPress blog, simply click 'New Post' above and get typing! Your posts will be listed on the right, and the red buttons can be used to delete posts or comments from your blog.";
    editDate();
  }
  
  /**
   * Post constructor
   * 
   * @param title Title of the post
   * @param content Content of the post
   */
  public Post(String title, String content) {
    this.title = title;
    this.content = content;
    this.blogger = Accounts.getCurrentUser();
    editDate();
  }
  
  /**
   * Formats the date input for output
   */
  public void editDate() {
    SimpleDateFormat formatter = new SimpleDateFormat("EEE d MMM yyyy");
    Date today = new Date();
    this.date = formatter.format(today); 
  }
}