package models;

import play.db.jpa.Model;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;

/**
 * @file Comment.java
 * @brief Class which describes a comment
 * @author michaelfoy
 * @version 2016-06-29
 */

@Entity
public class Comment extends Model {

  public String content;
  public String date;
  
  @ManyToOne
  public Post commentedPost;
  
  /**
   * Constructor for user comment
   * @param content Text of the comment
   */
  public Comment(String content) {
    this.content = content;
    editDate();
  }
  
  /**
   * Formats the date input for output
   */
  public void editDate() {
    SimpleDateFormat formatter = new SimpleDateFormat("h:mm a, EEE d MMM yyyy");
    Date today = new Date();
    this.date = formatter.format(today); 
  }
}