package controllers;

import play.*;
import play.mvc.*;
import java.util.*;
import models.*;

/**
 * @file Blog.java
 * @brief Controller for functionality of private blog
 * @author michaelfoy
 * @version 2016-07-19
 */
public class Blog extends Controller {

  /**
   * Loads blog post page. Takes a num parameter, if num set to default (zero),
   * first index of user.posts is loaded.
   * Otherwise num is used as post.id to find the post.
   * 
   * If user not logged in, num used to load public blog
   * 
   * @param num This number is used as index or post id to find post
   */
  public static void index(int num) {
    if (Accounts.getCurrentUser() != null) {
      List<User> bloggers = User.findAll();
      User user = Accounts.getCurrentUser();
      List<Post> posts = user.posts;
      Post post;

      if (num == 0) {
        post = user.posts.get(num);
      } else {
        Long longId = new Long(num);
        int index = user.posts.indexOf(Post.findById(longId));
        post = user.posts.get(index);
      }
      render(bloggers, user, post, posts);
    } else if (num > 0) {
      session.clear();
      String numString = num +"";
      PublicBlog.index(numString);     
    } else {
      session.clear();
      Logger.info("Returning to loooog in page");
      Accounts.login();
    }
    
  }

  /**
   * Loads new post page
   */
  public static void newpost() {
    if (Accounts.getCurrentUser() != null) {
      List<User> bloggers = User.findAll();
      render(bloggers);
    } else {
      session.clear();
      Accounts.index();
    }
  }

  /**
   * Creates a new post
   * 
   * @param title Title of the post
   * @param content Main text of the post
   */
  public static void createPost(String title, String content) {
    Post post = new Post(title, content);
    User user = Accounts.getCurrentUser();
    post.blogger = user;
    post.save();
    Logger.info("New post, title: " + post.title);
    index(0);
  }

  /**
   * Creates a new comment for the currently opened post
   * 
   * @param comment The text of the comment
   * @param postid Id of the relevant post
   */
  public static void comment(String comment, String postid) {
    Comment newComment = new Comment(comment);
    Post post = Post.findById(Long.parseLong(postid));
    newComment.commentedPost = post;
    newComment.save();
    
    int num = Integer.parseInt(post.blogger.id.toString());
    Logger.info("New comment for post: " + post.title);
    index(num);
  }
  
  /**
   * Deletes a post and all it's related comments.
   * 
   * @param postId Id number of the post
   * @param userId Id number of relevant user
   */
  public static void deletePost(String postId, String userId) {
    String check = session.get("logged_in_userid");
    User user = User.findById(Long.parseLong(userId));
    
    if (!userId.equals(check)) {
      session.clear();
      Accounts.index();
    } else {
      if (user.posts.size() > 1) {
        Post post = Post.findById(Long.parseLong(postId));
        Logger.info("Deleting post " + post.title);
        post.delete();
      }
      index(0);
    }
  }
  
  /**
   * Deletes an individual comment.
   * 
   * @param commentId Id of the comment to be deleted
   */
  public static void deleteComment(String commentId) {
    Comment comment = Comment.findById(Long.parseLong(commentId));
    Logger.info("Deleting comment: " + comment.content);
    comment.delete();
    index(0);   
  }
}
