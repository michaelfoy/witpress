package controllers;

import play.*;
import play.mvc.*;
import java.util.*;
import models.*;

/**
 * @file Accounts.java
 * @brief Controller for functionality of user accounts
 * @author michaelfoy
 * @version 2016-07-19
 */
public class Accounts extends Controller {

  /**
   * Loads landing page
   */
  public static void index() {
    List<User> users = User.findAll();
    render(users);
  }

  /**
   * Loads sign up page
   */
  public static void signup() {
    render();
  }

  /**
   * Loads log in page
   */
  public static void login() {
    render();
  }

  /**
   * Logs out current user, returns to landing page
   */
  public static void logout() {
    session.clear();
    index();
  }

  /**
   * Registers a new user, creates a welcome post for user
   * Logs in user to their account
   * 
   * @param firstName New user
   * @param lastName New user last name
   * @param email New user email
   * @param password New user password
   */
  public static void register(String firstName, String lastName, String email, String password) {
    Logger.info("Registering new user: " + firstName + " " + lastName);
    User newUser = new User(firstName, lastName, email, password);
    Post defaultPost = new Post();
    
    defaultPost.save();
    newUser.save();
    defaultPost.blogger = newUser;
    newUser.posts.add(defaultPost);
    defaultPost.save();
    newUser.save();
    
    session.put("logged_in_userid", newUser.id);
    Logger.info(newUser.firstName + " " + newUser.lastName + " logging in...");
    Blog.index(0);
  }

  /**
   * Verifies a registered user, if verified loads blog home page,
   * otherwise, returns to landing page
   * 
   * @param email User email to be verified
   * @param password User password to be verified
   */
  public static void authenticate(String email, String password) {
    User check = User.findByEmail(email);
    Logger.info("Attempting to authenticate with: " + email + " " + password);

    if ((check != null) && (check.checkPassword(password))) {
      session.put("logged_in_userid", check.id);
      Logger.info(check.firstName + " " + check.lastName + " logging in...");
      Blog.index(0);
    } else {
      Logger.info("Authentication failed");
      index();
    }
  }
  
  /**
   * If user logged in, returns that user,
   * otherwise loads home page, returns null
   * 
   * @return Currently logged in user, if available
   */
  public static User getCurrentUser() {
    String userid = session.get("logged_in_userid");
    if(userid != null) {
      User user = User.findById(Long.parseLong(userid));
      return user;
    } else {
      /*Logger.info("Redirecting to home page");
      Accounts.login();*/
      return null;
    }
  }
}
