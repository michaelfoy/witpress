package controllers;

import play.*;
import play.mvc.*;
import java.util.*;
import models.*;

/**
 * @file Blog.java
 * @brief Controller for functionality of public blogs
 * @author michaelfoy
 * @version 2016-07-19
 */
public class PublicBlog extends Controller {
  

  /**
   * Loads default public blog page from dropdown menu
   * 
   * @param userId Id of user, whose public blog is opened
   */
  public static void index(String userId) {
    User user = User.findById(Long.parseLong(userId));
    Post post = user.posts.get(0);
    List<Post> posts = user.posts;
    List<User> bloggers = User.findAll();
    render(bloggers, user, post, posts);
  }
  
  /**
   * Loads public blog post
   * 
   * @param userId Id of user whose public blog is opened
   * @param postId Id of blog post to be opened
   */
  public static void post(String userId, String postId) {
    User user = User.findById(Long.parseLong(userId));
    Post post = Post.findById(Long.parseLong(postId));
    List<Post> posts = user.posts;
    List<User> bloggers = User.findAll();
    render(bloggers, user, post, posts);
  }
}